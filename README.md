# NEAR Account keys, deploy & deletion scenarios

The following are some code samples for contract scenarios in NEAR that are either confusing, don't work as expected or could be implemented wrong but try to achieve a goal and can't.
NOTE: I'm trying to track down how things work, I'm assuming some of the patterns or implementations are not exactly the best way. Hoping my attempts can help future developers.

References:
https://nomicon.io/DataStructures/AccessKey.html
https://nomicon.io/RuntimeSpec/Actions.html#deleteaccountaction

## Prerequisite
Ensure `near-cli` is installed by running:

```
near --version
```

If needed, install `near-cli`:

```
npm install near-cli -g
```

## Building this contract
To make the build process compatible with multiple operating systems, the build process exists as a script in `package.json`.
There are a number of special flags used to compile the smart contract into the wasm file.
Run this command to build and place the wasm file in the `res` directory:
```bash
npm run build
```

**Note**: Instead of `npm`, users of [yarn](https://yarnpkg.com) may run:
```bash
yarn build
```

## Using this contract

```
// create account for the owner contract
near create-account vault.t.testnet --masterAccount t.testnet --initialBalance 1

near create-account case_tests.t.testnet --masterAccount t.testnet --initialBalance 1000


// deploy to created contract account
NEAR_ENV=testnet near deploy --wasmFile res/case_tests.wasm --accountId case_tests.t.testnet --masterAccount case_tests.t.testnet
```

## Case A: (Doesn't work, I believe it should)
REF: https://gitlab.com/TrevorJTClarke/near-account-crud-tests/-/blob/master/src/lib.rs#L36-48

* Contract creates a new account, adds signer full access key, transfer attached deposit to new account.
* Attempt to delete the new account, with CLI from signer

```
near call case_tests.t.testnet a --accountId t.testnet --gas 300000000000000 --amount 30
// view the created account id
near delete _new account id_ case_tests.t.testnet
near delete cases-284814421608153814319404013 case_tests.t.testnet
```

OUTCOME:
```
TypedError: Can not sign transactions for account cases-284814421608153814319404013 on network default, no matching key pair found in InMemorySigner...
```

-- TC: Doesnt exactly make sense because I gave full access key to the txn signer in the promise that created the account. Is this just a problem with my understanding of full access keys? Or is this a flaw in the near cli implementation on signing?

OUTCOME:

----

## Case B: (Worked! But why?)
REF: https://gitlab.com/TrevorJTClarke/near-account-crud-tests/-/blob/master/src/lib.rs#L51-64

* Contract creates a new account, adds contract full access key, transfer attached deposit to new account.
* Attempt to delete the new account via contract

near call case_tests.t.testnet b '{"pk":"ed25519:H669dnBgscYW5UDCmV9Eov9dHrmjKRL6CLtb4cfJaXdC"}' --accountId t.testnet --gas 300000000000000 --amount 30

OUTCOME:

```
Receipts: 5owUQPJCcSEHWYsU28Giztf2Npt834om8SLALVGrdnb4, Hhz1q87bBx5vWnD8Zb9o1Ncw4j1VA4JycqzTm978G2WR
	Log [case_tests.t.testnet]: Case B: cases-284829091608154717716872746
```

----

## Case C: (Doesn't work, I believe it should)
REF: https://gitlab.com/TrevorJTClarke/near-account-crud-tests/-/blob/master/src/lib.rs#L67-99

* Contract creates a new account, adds new full access key, transfer attached deposit to new account.
* deploy contract code to new account, with function to delete itself
* Attempt to delete the new account, using CLI to call contract del_c function
* Attempt to create new account again within same function promise set

```
near call case_tests.t.testnet c '{"pk":"ed25519:H669dnBgscYW5UDCmV9Eov9dHrmjKRL6CLtb4cfJaXdC"}' --accountId t.testnet --gas 300000000000000 --amount 30
near call case_tests.t.testnet del_c '{"account_id":"_new account id_","pk":"ed25519:H669dnBgscYW5UDCmV9Eov9dHrmjKRL6CLtb4cfJaXdC"}' --accountId t.testnet --gas 300000000000000 --amount 30
near call case_tests.t.testnet del_c '{"account_id":"cases-286571521608262740342725766","pk":"ed25519:H669dnBgscYW5UDCmV9Eov9dHrmjKRL6CLtb4cfJaXdC"}' --accountId t.testnet --gas 300000000000000 --amount 30
```

OUTCOME:
```
Scheduling a call: case_tests.t.testnet.del_c({"account_id":"cases-286571521608262740342725766","pk":"ed25519:H669dnBgscYW5UDCmV9Eov9dHrmjKRL6CLtb4cfJaXdC"}) with attached 30 NEAR
Receipts: G6y3m52uUvtnrFMuFuK7iSqDPhYRaX3ydp2DjPSpnZfU, 6EwwGuQJEnMxanWwPdYZMTyhsdAGtjXoP31JSv6X66SL, 7Hg7fLUw9MSyyYhErmak9VvQFHF17kZSV3TLjWkvr8Mk
	Log [case_tests.t.testnet]: Case C DEL: cases-286571521608262740342725766
Receipt: FqYMcr8zUZAu2jivA5hRg7Z5unHW8sVYyaV1pM4jHJZT
	Failure [case_tests.t.testnet]: Error: Actor case_tests.t.testnet doesn't have permission to account cases-286571521608262740342725766 to complete the action
Receipts: AFZyd7RPDV5zDHJPzTgn9uNZDsmeHuDDEymHWPJN3GU6, Dy8ex6rCZ2JKNB6KFKpVeLP1b8HaQAPzhWcPanbxg24q
	Failure [case_tests.t.testnet]: Error: Can't create a new account cases-286571521608262740342725766, because it already exists
An error occured
```

----

## Case D: (Doesn't work, I believe it should)
REF: https://gitlab.com/TrevorJTClarke/near-account-crud-tests/-/blob/master/src/lib.rs#L102-123

* Contract creates a new account with "testnet" contract, Full Access Key given to signer via function call.
* Attempt to delete the new account, with CLI from signer

```
near call case_tests.t.testnet d --accountId t.testnet --gas 300000000000000 --amount 30
// view the created account id
near delete _new account id_ vault.t.testnet
near delete cases-1608262464540128404.testnet vault.t.testnet
```

OUTCOME:
```
TypedError: Can not sign transactions for account cases-1608262464540128404.testnet on network default, no matching key pair found in InMemorySigner
```

-- TC: Same as Case A, i expect to have full access to delete but can't

----

## Case E: (Doesn't work, I believe it should)
REF: https://gitlab.com/TrevorJTClarke/near-account-crud-tests/-/blob/master/src/lib.rs#L126-149

* Contract creates a new account with "testnet" contract, Full Access Key given to contract via function call.
* Attempt to delete the new account via contract

```
near call case_tests.t.testnet e '{"pk":"ed25519:3Xuxb4ZoD1zKXEYu8JitBaz7pgQNAC4AYHHBxB7P62XD"}' --accountId t.testnet --gas 300000000000000 --amount 30
```

OUTCOME:
```
Scheduling a call: case_tests.t.testnet.e({"pk":"ed25519:3Xuxb4ZoD1zKXEYu8JitBaz7pgQNAC4AYHHBxB7P62XD"}) with attached 30 NEAR
Receipts: eRhQPDXKEc14cZdPndKc8KZY66pyZmskYoxY1i1uDQc, 9E2JRr5uqCvzVx9ynhAcvztEwT5aY4Nc9VHoJWitc3DY, AQuugZLv4UGuA9tjjNPJDENKx1kNgptT6KhhpuigBq2m
	Log [case_tests.t.testnet]: Case E: cases-1608155219610764435.testnet
Receipt: 46ao2aEvoXxfDQwR79h5taq31SA5XAgdYBhDBbMjiCMk
	Failure [case_tests.t.testnet]: Error: Actor case_tests.t.testnet doesn't have permission to account cases-1608155219610764435.testnet to complete the action
```

-- TC: Doesnt make sense, i am using the full access PK of case_tests.t.testnet, would this allow me to delete_account?

