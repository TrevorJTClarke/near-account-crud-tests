use borsh::{BorshDeserialize, BorshSerialize};
use near_sdk::serde_json::{json};
use near_sdk::{AccountId, Promise, env, near_bindgen};
use near_sdk::json_types::Base58PublicKey;
mod util;
use std::convert::{TryFrom};

// Ⓝa Ⓝa Ⓝa Ⓝa Ⓝa Ⓝa Ⓝa Ⓝa - Batmannnnnnnn
pub const ONE_NEAR: u128 = 1_000_000_000_000_000_000_000_000;
const MAX_GAS_FEE: u64 = 300_000_000_000_000;
const NEW_ACCOUNT_GAS_FEE: u64 = MAX_GAS_FEE / 2;
const NAME_CONTRACT: &str = "testnet";
const BENEFICIARY_ID: &str = "vault.t.testnet";

#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[near_bindgen]
#[derive(Default, BorshDeserialize, BorshSerialize)]
pub struct Cases {
}

#[near_bindgen]
impl Cases {

    pub fn del(&mut self) -> Promise {
        assert_ne!(
            env::current_account_id(),
            env::signer_account_id(),
        );
        Promise::new(env::current_account_id())
            .delete_account(BENEFICIARY_ID.to_string())
    }

    #[payable]
    pub fn a(&mut self) -> Promise {
        let account_id = format!(
            "{}{}{}",
            "cases-".to_string(),
            env::block_index().to_string(),
            env::block_timestamp().to_string(),
        );
        logger!("Case A: {}", &account_id);
        Promise::new(account_id.clone())
            .create_account()
            .add_full_access_key(env::signer_account_pk())
            .transfer(env::attached_deposit())
    }

    #[payable]
    pub fn b(&mut self, pk: Base58PublicKey) -> Promise {
        let account_id = format!(
            "{}{}{}",
            "cases-".to_string(),
            env::block_index().to_string(),
            env::block_timestamp().to_string(),
        );
        logger!("Case B: {}", &account_id);
        Promise::new(account_id.clone())
            .create_account()
            .add_full_access_key(pk.into())
            .transfer(env::attached_deposit())
            .delete_account(BENEFICIARY_ID.to_string())
    }

    #[payable]
    pub fn c(&mut self, pk: Base58PublicKey) -> Promise {
        let account_id = format!(
            "{}{}{}",
            "cases-".to_string(),
            env::block_index().to_string(),
            env::block_timestamp().to_string(),
        );
        logger!("Case C: {}", &account_id);
        Promise::new(account_id.clone())
            .create_account()
            .add_full_access_key(pk.into())
            .transfer(env::attached_deposit())
            .deploy_contract(
                include_bytes!("../res/case_tests.wasm").to_vec(),
            )
    }

    #[payable]
    pub fn del_c(&mut self, account_id: AccountId, pk: Base58PublicKey) -> Promise {
        assert_ne!(
            env::current_account_id(),
            env::signer_account_id(),
        );
        logger!("Case C DEL: {}", &account_id);
        let p1 = Promise::new(account_id.clone())
            .delete_account(BENEFICIARY_ID.to_string());
        let p2 = Promise::new(account_id.clone())
            .create_account()
            .add_full_access_key(pk.into())
            .transfer(env::attached_deposit());

        p1.then(p2)
    }

    #[payable]
    pub fn d(&mut self) -> Promise {
        let account_id = format!(
            "{}{}{}",
            "cases-".to_string(),
            env::block_timestamp().to_string(),
            ".testnet".to_string(),
        );
        logger!("Case D: {}", &account_id);
        let pkk = Base58PublicKey::try_from(env::signer_account_pk().to_vec()).unwrap();
        // let pk = Base58PublicKey::from(pkk);
        logger!("Case D: {:?}", &pkk);
        Promise::new(NAME_CONTRACT.to_string())
            .function_call(
                b"create_account".to_vec(),
                json!({
                    "new_account_id": &account_id,
                    "new_public_key": pkk,
                }).to_string().as_bytes().to_vec(),
                env::attached_deposit(),
                NEW_ACCOUNT_GAS_FEE
            )
    }

    #[payable]
    pub fn e(&mut self, pk: Base58PublicKey) -> Promise {
        let account_id = format!(
            "{}{}{}",
            "cases-".to_string(),
            env::block_timestamp().to_string(),
            ".testnet".to_string(),
        );
        logger!("Case E: {}", &account_id);
        let p1 = Promise::new(NAME_CONTRACT.to_string())
            .function_call(
                b"create_account".to_vec(),
                json!({
                    "new_account_id": &account_id,
                    "new_public_key": pk,
                }).to_string().as_bytes().to_vec(),
                env::attached_deposit(),
                NEW_ACCOUNT_GAS_FEE
            );

        let p2 = Promise::new(account_id)
            .delete_account(BENEFICIARY_ID.to_string());

        p1.then(p2)
    }
}

